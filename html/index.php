<?php 

session_start();
require('../php/connexionbdd.php');

if(empty($_SESSION['role_joueur'])) { $_SESSION['role_joueur'] ="new" ;} ;

?>


<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!-- liens Css -->
        <link rel="stylesheet" href="../css/bootstrap.css"/>
        <link rel="stylesheet" href="../css/style.css">
        <link rel="stylesheet" href="../css/roll.css">
        <title>Le Cercle Ecarlate</title>
        <!-- début Favicon -->
        <link rel="apple-touch-icon" sizes="57x57" href="../img/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="../img/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="../img/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="../img/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="../img/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="../img/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="../img/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="../img/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="../img/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="../img/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="../img/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="../img/favicon-16x16.png">
        <link rel="manifest" href="../img/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <!-- Fin Favicon -->
    </head>
    <body>

        <!-- Header + Navbar -->
        <?php include('header.php') ; ?>
        
        <?php include('nav.php') ; ?>

        <!-- container principal (main) -->
        <main>
            
            <!--  côté gauche page / salon de rolls -->
            <section id="rolls">
                <form id="lancers" onsubmit="return false;" >
                        <h2>Salon de Roll :</h2>
                        <ul id="lancer">
                            <li>
                                <p> Attaquant </p>
                                <div>
                                    <input type="image" alt="Dice" display="none" src="../img/roll.png" id="de1" onclick="rollForPlayer1();" />
                                    <p id="resultat_1"></p>
                                </div>
                            </li>
                            <li>
                                <p> Défenseur </p>
                                <div>
                                    <input type="image" alt="Dice" display="none" src="../img/roll.png" id="de2" onclick="rollForPlayer2(); afficheResultat();" /> 
                                    <p id="resultat_2"></p>
                                </div>
                            </li>
                        </ul>

                        <div id="resultats">

                        </div>

                        <div id="bouton">
                            <input type="image" id="refaire" src="../img/reset.png" alt="refaire" onclick="effaceResultats();" />
                        </div>
                
                </form>   
                
                <!-- côté droit page -->
                <aside>

                    <!-- prochaine date -->
                    <article id="next_date">
                        <h2>Prochain Combat :</h2>
                        <p>Date à entrer</p>
                    </article>

                    <!-- membres -->
                    <article id="membres">
                        <h2>Membres du Cercle :</h2>
                        <ul id="membre">
                            <!-- <li><figure><img src="../img/Lutys.png" alt="portrait" class="portrait"><figcaption>Lutys Al Bayda</figcaption></figure></li>
                            <li><figure><img src="../img/Lutys.png" alt="portrait" class="portrait"><figcaption>Lutys Al Bayda</figcaption></figure></li>
                            <li><figure><img src="../img/Lutys.png" alt="portrait" class="portrait"><figcaption>Lutys Al Bayda</figcaption></figure></li>
                            <li><figure><img src="../img/Lutys.png" alt="portrait" class="portrait"><figcaption>Lutys Al Bayda</figcaption></figure></li>
                            <li><figure><img src="../img/Lutys.png" alt="portrait" class="portrait"><figcaption>Lutys Al Bayda</figcaption></figure></li>
                            <li><figure><img src="../img/Lutys.png" alt="portrait" class="portrait"><figcaption>Lutys Al Bayda</figcaption></figure></li> -->
                           <?php $resultat=$bdd->query("SELECT * FROM personnages WHERE id_guildes = 2");
                                    $resultat->setFetchMode(PDO::FETCH_ASSOC);
                                        foreach ($resultat as $perso){
                                        echo '<li><figure><img src="../uploads/'. $perso['photo'] .' " alt="portrait" class="portrait"><figcaption> ' . $perso['nom_perso'] . ' &nbsp ' . $perso['prenom_perso'] . '</figcaption></figure></li>';
                                        };
                            ?>
                        </ul>
                    </article>
                </aside>
            </section>       
            

        </main>

        <!-- footer -->
        <?php include('footer.php'); ?>

        <!-- script du salon de roll -->
        <script src="../js/roll.js"></script>

    </body>
    
</html>
 



