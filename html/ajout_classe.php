<?php 
session_start();
if(!isset($_SESSION['role_joueur']) || $_SESSION['role_joueur'] != 'admin') {
    echo "pas d'accès";
    exit();
}
include ('../php/connexionbdd.php');
require('../php/ajout_classe_post.php');
if (!empty($classe_ok)) header("Refresh: 3;url=accueil.php");
?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="../css/bootstrap.css"/>
        <link rel="stylesheet" href="../css/style.css" />
        <title>Nouvelle Classe</title>
        <!-- début Favicon -->
        <link rel="apple-touch-icon" sizes="57x57" href="../img/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="../img/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="../img/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="../img/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="../img/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="../img/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="../img/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="../img/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="../img/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="../img/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="../img/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="../img/favicon-16x16.png">
        <link rel="manifest" href="../img/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <!-- Fin Favicon -->
    </head>
    <body>
        
        <?php require 'header.php'; ?>

        <main>
     

            <form id="monformulaire5" name="monformulaire5" action="ajout_classe.php" method="post">
                <div id="group1">
                    <fieldset id="bloc1">
                        <legend>Ajout d'une Classe</legend>

                        <label for="classe">Nom Classe
                        <input type="text" name="classe" id="classe" value="<?php if (isset($_POST['classe'])){echo $_POST['classe'];} ?>" /></label>

                    </fieldset>
                </div>
                <div id="message_erreur">
                    <?php 
                        if (!empty($erreur_classe)) echo "$erreur_classe";
                        if (!empty($classe_ok)) echo "$classe_ok";
                    ?>
                </div>
                <div id="boutons">
                <button type="reset" value="reset" alt="effacer" name="reset" class="btn btn-warning" id="refaire">Effacer</button>
                <button type="submit" value="submit" alt="valider" name="submit" class="btn btn-success" id="validation">Valider</button>
                </div>
            </form>  
        
        </main>

        <?php require 'footer.php'; ?>

        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
        <script src="../js/bouton.js"></script>
    </body>

</html>