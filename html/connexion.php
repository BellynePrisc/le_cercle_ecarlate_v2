<?php
session_start();
require('../php/connexion_post.php');
if ($validation == true) header("refresh: 2; url=accueil.php");
?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="../css/bootstrap.css"/>
        <link rel="stylesheet" href="../css/style.css">
        <link rel="stylesheet" href="../css/inscrip_connec.css">
        <title>Connexion</title>
        <!-- début Favicon -->
        <link rel="apple-touch-icon" sizes="57x57" href="../img/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="../img/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="../img/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="../img/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="../img/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="../img/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="../img/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="../img/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="../img/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="../img/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="../img/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="../img/favicon-16x16.png">
        <link rel="manifest" href="../img/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <!-- Fin Favicon -->
    </head>
    <body>

        <!-- HEADER -->
        <?php require 'header.php'; ?>

        
        <div class="jumbotron">

            <!-- FORMULAIRE DE CONNEXION -->
            <form id="monformulaire2" action="connexion.php" method="post">
                <fieldset id="bloc1">
                    <legend>Vos Identifiants</legend>

                    <label for="login">Votre Nom
                    <input type="text" name="login" id="login" value="<?php if (isset($_POST['login'])){echo $_POST['login'];} ?>"/></label>

                    <label for="password">Votre Mot de Passe
                    <input type="password" name="password" id="password" value="<?php if (isset($_POST['password'])){echo $_POST['password'];} ?>"/></label>

                </fieldset>

                <!-- MESSAGES D'ERREUR -->
                <div id="message_erreur">
                    <?php 
                        if (!empty($erreur_nom)) echo "$erreur_nom"; 
                        if (!empty($erreur_mdp)) echo "$erreur_mdp";
                        if (isset($_SESSION['erreurs'])) {
                            echo "<p>".$_SESSION['erreurs']."</p>";
                            $_SESSION['erreurs']="";
                        };
                        if (!empty($valid_log)) echo "$valid_log";
                    ?>
                </div>

                <!-- BOUTONS -->
                <div id="boutons">
                <button type="reset" value="reset" alt="effacer" name="reset" class="btn btn-warning" id="refaire">Effacer</button>
                <button type="submit" value="submit" alt="valider" name="submit" class="btn btn-success" id="validation">Valider</button>
                <a href="inscription.php"><button type="button" value="redirection" alt="redirection" name="redirection" class="btn btn-info" id="redirection">S'Inscrire</button></a>

                </div>
            </form> 
        
        </div>   

        <!-- FOOTER -->
        <?php require 'footer.php'; ?>

        <!-- SCRIPT -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
        <script src="../js/bouton.js"></script>
    </body>
</html>