//ADDITION SIMPLE
//Points Magie
$('#volonte_perso, #intelligence_perso').on("change input", function(){
    $('#points_magie').val(
        parseInt($('#volonte_perso').val()) + parseInt($('#intelligence_perso').val())
    );
});


//MOYENNE: Addition puis division avec arrondi à l'entier inférieur
//Attaque_Cac
$('#force_perso, #dexterite_perso').on("change input", function(){
    $('#attaque_cac').val(
        Math.floor((parseInt($('#force_perso').val()) + parseInt($('#dexterite_perso').val()))/2)
    );
});

//Attaque_Dist
$('#perception_perso, #dexterite_perso').on("change input", function(){
    $('#attaque_dist').val(
        Math.floor((parseInt($('#perception_perso').val()) + parseInt($('#dexterite_perso').val()))/2)
    );
});

//Attaque_Magie_Dist
$('#perception_perso, #intelligence_perso').on("change input", function(){
    $('#attaque_magie_dist').val(
        Math.floor((parseInt($('#perception_perso').val()) + parseInt($('#intelligence_perso').val()))/2)
    );
});

//Blocage
$('#constitution_perso, #dexterite_perso').on("change input", function(){
    $('#blocage').val(
        Math.floor((parseInt($('#constitution_perso').val()) + parseInt($('#dexterite_perso').val()))/2)
    );
});

//Parade
$('#force_perso, #dexterite_perso').on("change input", function(){
    $('#parade').val(
        Math.floor((parseInt($('#force_perso').val()) + parseInt($('#dexterite_perso').val()))/2)
    );
});

//Esquive
$('#rapidite_perso, #dexterite_perso').on("change input", function(){
    $('#esquive').val(
        Math.floor((parseInt($('#rapidite_perso').val()) + parseInt($('#dexterite_perso').val()))/2)
    );
});


//ATTRIBUTION DE VALEURS IDENTIQUES A DES INPUTS VERROUILLES
//Attaque_magie
$('#intelligence_perso').on("change input", function(){
    $('#attaque_magie_cac').val(
        parseInt($('#intelligence_perso').val())
    );
});

//Dégâts càc
$('#force_perso').on("change input", function(){
    $('#degat_cac').val(
        parseInt($('#force_perso').val())
    );
});

//Dégâts Dist
$('#dexterite_perso').on("change input", function(){
    $('#degat_dist').val(
        parseInt($('#dexterite_perso').val())
    );
});

//Dégâts Magie
$('#intelligence_perso').on("change input", function(){
    $('#degat_magie').val(
        parseInt($('#intelligence_perso').val())
    );
});

//Résistance Physique
$('#constitution_perso').on("change input", function(){
    $('#resist_physique').val(
        parseInt($('#constitution_perso').val())
    );
});

//Résistance Magie
$('#volonte_perso').on("change input", function(){
    $('#resist_magie').val(
        parseInt($('#volonte_perso').val())
    );
});
