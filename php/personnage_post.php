<?php 
    // session_start();
    
    //Connexion à la Bdd 
    include('connexionbdd.php');
    // var_dump($_GET);

    if (isset($_POST['submit'])){

        
        
        //on vérifie les données avant de les inscrire dans la table
        $message_champ = "";
        $erreur = "";

        
        //on nomme les champs pour l'affichage dans le message d'erreur ( K => V)
        $champs = [
            'nom_perso' => 'Nom',
            'prenom_perso' => 'Prénom',
            'age_perso' => 'Âge',
            'metier' => 'Métier', 
            'classe' => 'Classe',
            'guilde' => 'Guilde',
            'role_guilde' => 'Role de Guilde', 
            'force_perso' => 'Force', 
            'constitution_perso' => 'Constitution', 
            'dexterite_perso' => 'Dextérité', 
            'intelligence_perso' => 'Intelligence', 
            'charisme_perso' => 'Charisme', 
            'perception_perso' => 'Perception', 
            'volonte_perso' => 'Volonté', 
            'rapidite_perso' => 'Rapidité', 
            'points_vie' => 'Points de Vie'   
        ];

        //on boucle pour vérifier les champs en donnant le K=>V du tableau de champs
        foreach ($champs as $key => $champ) {
            if(!isset($_POST[$key]) OR empty($_POST[$key])){
                $erreur .= $champ.", ";
            }
        }
        
        // on retire la dernière virgule
        $erreur = substr($erreur, 0, -2);  

        //on affiche le message d'erreur des champs vides
        $message_champ .= '<p> Veuillez remplir les champs : '.$erreur;

        $message_upload = "";
        //on vérifie que le fichier photo n'est pas vide
        if($_FILES['photo']['size']== 0)  {
            $message_champ .= ', Photo';
        }
        else {
            $target_Path = "../uploads/";
            $target_Path = $target_Path.basename( $_FILES['photo']['name'] );
            move_uploaded_file( $_FILES['photo']['tmp_name'], $target_Path );
            $message_upload .= '<p> Image téléchargée avec succès </p>';

        }
        
        $message_champ .= '</p>';
    

        //On inscrit les données en étant sûr qu'il n'y a aucun champ manquant. 
        if(($erreur == "") AND !empty($message_upload)) {

            //On récupère l'id du joueur
            $joueur= "";
            $reponse = $bdd->prepare('SELECT id FROM joueurs WHERE login = ?');
            $reponse->execute(array($_SESSION['login']));
            $donnee = $reponse->fetch();
            $joueur = $donnee['id'];

            //TABLE DESCRIPTIONS
            //On crée l'array à insérer 
            $data = [
                'arme_cac' => $_POST['arme_cac'],
                'arme_dist' => $_POST['arme_dist'],
                'inventaire' => $_POST['inventaire'],
                'physique' => $_POST['physique'],
                'divers' => $_POST['divers']
            ];


                //On insère les données dans la BDD et on récupère le dernier ID inséré
                $description = "";
                $sql = "INSERT INTO descriptions (arme_cac, arme_dist, inventaire, physique, divers) VALUES (:arme_cac, :arme_dist, :inventaire, :physique, :divers)";
                $stmt = $bdd->prepare($sql);

                    if(true == $stmt->execute($data)) {
                        $description = $bdd->lastInsertId();
                    }


            

            //TABLE COMPETENCES
            //On crée l'array à insérer
            $data2 = [
                'attaque_cac' => $_POST['attaque_cac'],
                'attaque_dist' => $_POST['attaque_dist'],
                'attaque_magie_cac' => $_POST['attaque_magie_cac'],
                'attaque_magie_dist' => $_POST['attaque_magie_dist'],
                'degat_cac' => $_POST['degat_cac'],
                'degat_dist' => $_POST['degat_dist'],
                'degat_magie' => $_POST['degat_magie'],
                'resist_physique' => $_POST['resist_physique'],
                'resist_magie' => $_POST['resist_magie'],
                'blocage' => $_POST['blocage'],
                'parade' => $_POST['parade'],
                'esquive' => $_POST['esquive']
            ];


                //On insère les données dans la BDD et on récupère le dernier ID inséré
                $competence = "";
                $sql = "INSERT INTO competences (attaque_cac, attaque_dist, attaque_magie_cac, attaque_magie_dist, degat_cac, degat_dist, degat_magie, resist_physique, resist_magie, blocage, parade, esquive) VALUES (:attaque_cac, :attaque_dist, :attaque_magie_cac, :attaque_magie_dist, :degat_cac, :degat_dist, :degat_magie, :resist_physique, :resist_magie, :blocage, :parade, :esquive)";
                $stmt = $bdd->prepare($sql);

                    if(true == $stmt->execute($data2)) {
                        $competence = $bdd->lastInsertId();
                    }

                

            //TABLE CARACTERISTIQUES
            //On crée l'array à insérer
            $data3 = [
                'force_perso' => $_POST['force_perso'],
                'constitution_perso' => $_POST['constitution_perso'],
                'dexterite_perso' => $_POST['dexterite_perso'],
                'intelligence_perso' => $_POST['intelligence_perso'],
                'charisme_perso' => $_POST['charisme_perso'],
                'perception_perso' => $_POST['perception_perso'],
                'volonte_perso' => $_POST['volonte_perso'],
                'rapidite_perso' => $_POST['rapidite_perso']
            ];

                //On insère les données dans la BDD et on récupère le dernier ID inséré
                $caracteristique = "";
                $sql = "INSERT INTO caracteristiques (force_perso, constitution_perso, dexterite_perso, intelligence_perso, charisme_perso, perception_perso, volonte_perso, rapidite_perso) VALUES (:force_perso, :constitution_perso, :dexterite_perso, :intelligence_perso, :charisme_perso, :perception_perso, :volonte_perso, :rapidite_perso)";
                $stmt = $bdd->prepare($sql);

                    if(true == $stmt->execute($data3)) {
                        $caracteristique = $bdd->lastInsertId();
                    }


            //TABLE PERSONNAGES
            //On crée l'array à insérer
            $data4 = [
                'id_joueurs' => $joueur,
                'nom_perso' => $_POST['nom_perso'],
                'prenom_perso' => $_POST['prenom_perso'],
                'age_perso' => $_POST['age_perso'],
                'metier' => $_POST['metier'],
                'id_classes' => $_POST['classe'],
                'photo' => $_FILES['photo']['name'],
                'id_guildes' => $_POST['guilde'],
                'role_guilde' => $_POST['role_guilde'],
                'id_caracteristiques' => $caracteristique, 
                'points_vie' => $_POST['points_vie'],
                'points_magie' => $_POST['points_magie'],
                'id_competences' => $competence,
                'id_descriptions' => $description
            ];

                //on crée la variable qui vérifiera la création
                $personnage = "";
                //On insère les données dans la BDD et on affiche une alerte de réussite à l'utilisateur
                $sql = "INSERT INTO personnages (id_joueurs, nom_perso, prenom_perso, age_perso, metier, id_classes, photo, id_guildes, role_guilde, id_caracteristiques, points_vie, points_magie, id_competences, id_descriptions) VALUES (:id_joueurs, :nom_perso, :prenom_perso, :age_perso, :metier, :id_classes, :photo, :id_guildes, :role_guilde, :id_caracteristiques, :points_vie, :points_magie, :id_competences, :id_descriptions)";
                $stmt = $bdd->prepare($sql);
                if(true == $stmt->execute($data4)) {
                    $personnage .= "<p>Votre fiche pour ".$_POST['prenom_perso']." a bien été créée !</p>";
                }

        }
        
    }

?>




