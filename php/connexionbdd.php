<?php
// Connexion à la base de données

// try
// {
// 	$bdd = new PDO('mysql:host=localhost;dbname=annuaire;charset=utf8', 'root', '');
// }
// catch(Exception $e)
// {
//         die('Erreur : '.$e->getMessage());
// }

        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "annuaire";

        try {
                $bdd = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch(PDOException $e)  {
                echo "Connection failed: " . $e->getMessage();
            }

?>