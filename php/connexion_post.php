<?php 

    $validation = false;

    if (isset($_POST['submit'])){

        //Connexion à la Bdd 
        include ('connexionbdd.php');

        // On vérifie que le login existe dans la base
        $erreur_login = "";
        $reponse = $bdd->query('SELECT COUNT(*) AS `compteur` FROM `joueurs` WHERE `login` = "'.$_POST['login'].'"');
        $donnee = $reponse->fetch();
        $compteur = $donnee['compteur'];
            if ($compteur == 0) {
                $erreur_login .= "<p>Ce Nom n'existe pas !</p>";
            }

        // On verifie que le mdp saisit corresponde au mdp du login dans la base
        $erreur_mdp = "";
        $reponse2 = $bdd->prepare('SELECT password FROM joueurs WHERE login = ?');
        $reponse2->execute(array($_POST['login']));
        $donnee2 = $reponse2->fetch();
        $mdp = $donnee2['password'];
            if (!password_verify($_POST['password'], $mdp)){
            $erreur_mdp .= "<p>Le mot de passe ne correspond pas à celui enregistré !</p>";
            }

        // on récupère les données pour en faire les variables de session
        $valid_log = "";
        $req = $bdd->prepare('SELECT `login`, `role_joueur` FROM `joueurs` WHERE `login` = ? ');   // effacement de l'interrogation du MDP vu qu'il a été vérifié au dessus
        $req->execute(array($_POST['login']));
        $donnee3 = $req->fetch();
        $_SESSION['login'] = $donnee3['login'];
        $_SESSION['role_joueur'] = $donnee3['role_joueur'];
            if ((!empty($donnee3['login'])) AND (!empty($donnee3['role_joueur']))) {
                $valid_log .= "<p>Votre session ".$_SESSION['login']." a bien été ouverte !</p>"; 
            }

        //s'il n'y a pas d'erreur et que le log de validation est remplit, on valide
        if (($erreur_login == "") AND ($erreur_mdp == "") AND (!empty($valid_log))) $validation = true;  
            
    }


?>