<?php 
// session_start();
include('connexionbdd.php');
include('affiche_personnage_simple.php');

// var_dump($fiche_id);
// var_dump($SESSION['fiche_id']);
// var_dump($resultat['description_id']);
$fiche = $_SESSION['id_fiche'];

// On vérifie que le formulaire est valide et qu'il détient un id de fiche
if (isset($_POST['submit']) && (!empty($fiche))) {

    //on UPDATE la table DESCRIPTIONS
  /*  $requete=$bdd->prepare('UPDATE descriptions
                            SET arme_cac = :arme_cac,
                                arme_dist = :arme_dist,
                                inventaire = :inventaire, 
                                physique = :physique, 
                                divers = :divers
                            WHERE id = \''.$resultat['description_id'].'\'');
                            
    $requete->execute(array(
            'arme_cac' => $_POST['arme_cac'],
            'arme_dist' => $_POST['arme_dist'],
            'inventaire' => $_POST['inventaire'],
            'physique' => $_POST['physique'],
            'divers' => $_POST['divers']));*/
    $sql = 'UPDATE descriptions 
            SET arme_cac = "'.$_POST['arme_cac'].'", 
                arme_dist = "'.$_POST['arme_dist'].'", 
                inventaire = "'.$_POST['inventaire'].'",
                physique = "'.$_POST['physique'].'",
                divers = "'.$_POST['divers'].'"
            WHERE id = "'.$resultat['description_id'].'"
    ';
    $bdd->exec($sql);


    //on UPDATE la table COMPETENCES
    $requete=$bdd->prepare('UPDATE competences
                            SET attaque_cac = :attaque_cac,
                                attaque_dist = :attaque_dist,
                                attaque_magie_cac = :attaque_magie_cac, 
                                attaque_magie_dist = :attaque_magie_dist,
                                degat_cac = :degat_cac,
                                degat_dist = :degat_dist,
                                degat_magie = :degat_magie,
                                resist_physique = :resist_physique,
                                resist_magie = :resist_magie,
                                blocage = :blocage, 
                                parade = :parade, 
                                esquive = :esquive
                            WHERE id = \''.$resultat['competence_id'].'\'');
                            
    $requete->execute(array(
            'attaque_cac' => $_POST['attaque_cac'],
            'attaque_dist' => $_POST['attaque_dist'],
            'attaque_magie_cac' => $_POST['attaque_magie_cac'],
            'attaque_magie_dist' => $_POST['attaque_magie_dist'],
            'degat_cac' => $_POST['degat_cac'],
            'degat_dist' => $_POST['degat_dist'],
            'degat_magie' => $_POST['degat_magie'],
            'resist_physique' => $_POST['resist_physique'],
            'resist_magie' => $_POST['resist_magie'],
            'blocage' => $_POST['blocage'],
            'parade' => $_POST['parade'],
            'esquive' => $_POST['esquive']));




    //on UPDATE la table CARACTERISTIQUES
    $requete=$bdd->prepare('UPDATE caracteristiques
                            SET force_perso = :force_perso,
                                constitution_perso = :constitution_perso,
                                dexterite_perso = :dexterite_perso, 
                                intelligence_perso = :intelligence_perso,
                                charisme_perso = :charisme_perso,
                                perception_perso = :perception_perso,
                                volonte_perso = :volonte_perso, 
                                rapidite_perso = :rapidite_perso
                            WHERE id = \''.$resultat['caracteristique_id'].'\'');
                            
    $requete->execute(array(
            'force_perso' => $_POST['force_perso'],
            'constitution_perso' => $_POST['constitution_perso'],
            'dexterite_perso' => $_POST['dexterite_perso'],
            'intelligence_perso' => $_POST['intelligence_perso'],
            'charisme_perso' => $_POST['charisme_perso'],
            'perception_perso' => $_POST['perception_perso'],
            'volonte_perso' => $_POST['volonte_perso'],
            'rapidite_perso' => $_POST['rapidite_perso']));




    //on crée une variable pour l'update de la photo afin de l'enregistrer en BDD
    $nom_photo ="";
    
    //On vérifie la photo du formulaire et on lui attribue une valeur pour l'update
    if(($_FILES['photo']['size'] !== 0) || ($_FILES['photo']['name'] !== $resultat['photo'])) {
        $target_Path = $dir.basename( $_FILES['photo']['name'] );
        move_uploaded_file( $_FILES['photo']['tmp_name'], $target_Path );
        $nom_photo = $_FILES['photo']['name'];
    } 
    else {
        $nom_photo = $resultat['photo'];
    }




    //on UPDATE la table PERSONNAGES
    $requete=$bdd->prepare('UPDATE personnages
                            SET nom_perso = :nom_perso,
                                prenom_perso = :prenom_perso,
                                age_perso = :age_perso, 
                                metier = :metier,
                                id_classes = :classe, 
                                photo = :photo,
                                id_guildes = :guilde, 
                                role_guilde = :role_guilde,
                                points_vie = :points_vie,
                                points_magie = :points_magie 
                            WHERE id = \''.$fiche.'\'');
                            
    $requete->execute(array(
            'nom_perso' => $_POST['nom_perso'],
            'prenom_perso' => $_POST['prenom_perso'],
            'age_perso' => $_POST['age_perso'],
            'metier' => $_POST['metier'],
            'classe' => $_POST['classe'],
            'photo' => $nom_photo,
            'guilde' => $_POST['guilde'],
            'role_guilde' => $_POST['role_guilde'],
            'points_vie' => $_POST['points_vie'],
            'points_magie' => $_POST['points_magie']));

// }
// else {
//     header("refresh: 2; url=accueil.php");
};

?>
