<?php
    session_start();

    if (isset($_POST['submit'])) {
        
        //connexion à la bdd 
        include ('connexionbdd.php');

        //on vérifie les données avant de les inscrire dans la table
        $message_erreur = "";
        $message_champ = "";
        $erreur = "";
        $champs = ['login', 'compte_joueur', 'email', 'password', 'mdpVerif'];
        foreach ($champs as $champ) {
            if(!isset($_POST[$champ]) OR empty($_POST[$champ])){
                $erreur .= $champ.", ";
            }
        }
        $erreur = substr($erreur, 0, -2);  // on retire la dernière virgule

        // On vérifie que les champs requis ( login, email) n'existent pas dans la table 
                // verif du mail
            $erreur_mail = "";
            $reponse = $bdd->query('SELECT COUNT(*) AS `compteur` FROM `joueurs` WHERE `email` = "'.$_POST['email'].'"'); 
            $donnee = $reponse->fetch();
            $compteur = $donnee['compteur'];
                if ($compteur > 0 ) {
                    $erreur_mail .= "Adresse email déjà utilisée !";
                }
                // verif du login
            $erreur_login = "";
            $reponse = $bdd->query('SELECT COUNT(*) AS `compteur2` FROM `joueurs` WHERE `login` = "'.$_POST['login'].'"');
            $donnee = $reponse->fetch();
            $compteur2 = $donnee['compteur2'];
                if ($compteur2 > 0) {
                    $erreur_login .= "Nom déjà utilisé !";
                }
            // verif que le password corresponde au mdpVerif
            $erreur_mdp = "";
                if ($_POST['mdpVerif'] != $_POST['password']) $erreur_mdp .= "Les mots de passe ne sont pas identiques !";
            
            // On affiche un message d'erreur en cas de doublon dans la BDD ou 
            //de non remplissage des champs requis ou que les mdp ne sont pas identiques
            $message_champ .= '<p> Veuillez remplir les champs : '.$erreur.'</p>';
            $message_erreur .= '<p>'.$erreur_mail.'</p>'.'<p>'.$erreur_login.'</p>'.'<p>'.$erreur_mdp.'</p>';

        // inscription des données dans la table "personnes"
        $creation = "";
        if (($erreur == "") AND ($compteur == 0) AND ($compteur2 == 0) AND (empty($erreur_mdp))) {
            // var_dump('ok');
            // die;
            $req = $bdd->prepare("INSERT INTO joueurs (login, compte_joueur, email, password, role_joueur) VALUES (?, ?, ?, ?, 'inscrit')");
            $req->execute(array(
                $_POST['login'],
                $_POST['compte_joueur'],
                $_POST['email'],
                password_hash($_POST['password'], PASSWORD_DEFAULT)
            )); 
        $creation .= "<p>Votre Compte ".$_POST['login']." a bien été créé !</p>";
        $_SESSION['role_joueur'] = "inscrit";
        $_SESSION['login'] = $_POST['login'];
        }
    }

?>